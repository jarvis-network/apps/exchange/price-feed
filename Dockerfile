ARG BASE_IMAGE=alpine

# ----------------------------------- Base ----------------------------------- #
FROM $BASE_IMAGE AS base
RUN apk --no-cache add build-base coreutils dub ldc openssl-dev zlib-dev
WORKDIR /src

# ------------------------ Gather dub package metadata ----------------------- #
FROM base as dub-cache
COPY . .
RUN mkdir /out \
  && find . -name "dub.sdl" -o -name "dub.json" -o -name "dub.selections.json" | \
    xargs cp -v --parents -t /out \
# Create empty D files for each app to avoid the warning '...package <name> contains no source files...'
  && mkdir -p /out/apps/listener/source && echo -e 'module app;\nvoid main() {}' > /out/apps/listener/source/app.d \
  && mkdir -p /out/apps/emitter/source && echo -e 'module app;\nvoid main() {}' > /out/apps/emitter/source/app.d \
  && mkdir -p /out/apps/seed/source && echo -e 'module app;\nvoid main() {}' > /out/apps/seed/source/app.d \
  ;

# ----------------------- Fetch and build dependencies ----------------------- #
FROM base AS dependencies-cache
COPY --from=dub-cache /out .
RUN dub build --parallel


# --------------------------------- Build All -------------------------------- #
FROM dependencies-cache AS app-builder
COPY apps ./apps/
COPY libs ./libs/
RUN dub build --parallel
RUN mkdir /out && cp -v /src/apps/*/build/* /out

# ---------------------------------------------------------------------------- #
#                                Generic Runner                                #
# ---------------------------------------------------------------------------- #
FROM $BASE_IMAGE as runner-cache
RUN apk --no-cache add ldc-runtime libexecinfo libgcc tzdata
EXPOSE 8080
WORKDIR /app

FROM runner-cache AS runner
ARG APP_NAME
RUN echo -e "#!/bin/sh\n./price-feed_${APP_NAME}" > '/app/start.sh' && chmod +x '/app/start.sh'
COPY --from=app-builder /out/price-feed_${APP_NAME} .
ENTRYPOINT ["/app/start.sh"]
