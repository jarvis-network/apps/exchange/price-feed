#!/bin/sh

set -e
option="${1}"
default_version='0.0.0-SNAPSHOT'
next_version="${2:-$default_version}"
case ${option} in
   --prepare)
      # Publish Docker in Github Packages
      echo $PWD
      TAG="$next_version"
      docker buildx bake emitter listener
      sed -i "/tag:/c\  tag: \"$(echo $TAG)\"" $PWD/helm/emitter/values.yaml
      git add $PWD/helm/emitter/values.yaml
      sed -i "/tag:/c\  tag: \"$(echo $TAG)\"" $PWD/helm/listener/values.yaml
      git add $PWD/helm/listener/values.yaml
      sed -i 's/\(price-feed:seed\).*/\1" version=\">='$next_version'\"/g' dub.sdl
      sed -i 's/\(price-feed:emitter\).*/\1" version=\">='$next_version'\"/g' dub.sdl
      sed -i 's/\(price-feed:listener\).*/\1" version=\">='$next_version'\"/g' dub.sdl
      git add dub.sdl
      ;;
   *)
      ;;
esac
