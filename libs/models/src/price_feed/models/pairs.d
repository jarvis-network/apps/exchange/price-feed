module price_feed.models.pairs;

import std.traits : EnumMembers;

/// Supported symbols
enum Pair
{
    EURUSD,
    GBPUSD,
    USDCHF,
    XAUUSD,
}

/// Helper array with the supported symbols as strings
static const stringPairs = [__traits(allMembers, Pair)];

/// Helper array with the supported symbols as enum members
static const enumPairs = [EnumMembers!Pair];
