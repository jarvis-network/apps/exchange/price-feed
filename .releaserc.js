module.exports = {
  branches: [ 'various-improvements' ],
  tagFormat: 'v${version}-pf',
  debug: true,
  ci: false,
  dryRun: false,
  preset: 'angular',
  plugins: [
    '@semantic-release/commit-analyzer',
    '@semantic-release/release-notes-generator',
    [
      '@semantic-release/changelog',
      {
        changeLogFile: 'CHANGELOG.md'
      }
    ],
    [
      '@semantic-release/exec',
      {
        prepareCmd: './release.sh --prepare ${nextRelease.version}'
      }
    ],
    [
      '@semantic-release/git',
      {
        asset: ['CHANGELOG.md', 'package.json'],
        message: 'chore(release): ${nextRelease.version} [skip ci] release notes \n\n ${nextRelease.notes}'
      }
    ]
  ]
}
