{ stdenv, buildGoModule, fetchFromGitHub }:

# Based on: https://github.com/NixOS/nixpkgs/pull/107302/files#diff-7f70026130f8256f5bde228744038886c197e08b7e805440edff09dc65d656ea

buildGoModule rec {
  pname = "docker-buildx";
  version = "0.5.1";
  rev = "v${version}";

  src = fetchFromGitHub {
    owner = "docker";
    repo = "buildx";
    rev = "v${version}";
    sha256 = "0l03ncs1x4lhgy0kf7bd1zq00md8fi93f8xq6k0ans4400divfzk";
  };

  vendorSha256 = null;

  installPhase = ''
    install -D $GOPATH/bin/buildx $out/libexec/docker/cli-plugins/docker-buildx
  '';

  meta = with stdenv.lib; {
    description = "Docker CLI plugin for extended build capabilities with BuildKit";
    license = licenses.asl20;
    maintainers = [ maintainers.ivan-babrou ];
    platforms = with platforms; linux ++ darwin;
  };
}
