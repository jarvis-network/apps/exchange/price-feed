#!/usr/bin/env rdmd

import std.exception : enforce;
import std.file : isFile, readText;
import std.json;
import std.stdio;

void main(string[] args)
{
    enforce(
        args.length == 2 && isFile(args[1]),
        "Usage:\n\n    json_to_nix <JSON-FILE-PATH>"
    );

    const json = args[1].readText.parseJSON;

    json.fromJsonToNix.writeln;
}

string fromJsonToNix(JSONValue j, uint level = 1)
{
    import std.array : replicate;
    import std.conv : text;

    final switch (j.type) with (JSONType)
    {
        case integer:
        case uinteger:
        case float_:
        case null_:
        case true_:
        case false_:
        case string:
            return j.toString(JSONOptions.doNotEscapeSlashes);
        case array:
            .string result = "[ ";
            foreach (idx, value; j.array)
            {
                if (idx != 0)
                    result ~= " " ~ fromJsonToNix(value, level + 1);
                else
                    result ~= fromJsonToNix(value, level + 1);
            }
            result ~= " ]";
            return result;
        case object:
            .string result = "{\n";
            foreach (key, value; j.object)
                result ~= text("  ".replicate(level), key, " = ", fromJsonToNix(value, level + 1), ";\n");
            result ~= "  ".replicate(level - 1) ~ "}";
            return result;
    }
}
