(import ./docker-bake.nix) {
  registryUrl = "jarvisnetworkcoreacr.azurecr.io/jarvis-network/apps/exchange/price-feed";
  serviceNamespace = "price-feed";
}
