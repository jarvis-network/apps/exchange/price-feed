#!/usr/bin/env -S rdmd -preview=shortenedMethods

import std.conv : to;
import std.format : fmt = format;
import std.json : J = JSONValue, JSONOptions;
import std.process : environment;
import std.stdio : writeln;

bool local() => environment.get("DOCKER_BUILD_LOCAL", "false").to!bool;
string tag() => environment["IMAGE_TAG"];
string depsTag() => environment["DEPS_IMAGE_TAG"];
string registryUrl() => environment["REGISTRY_URL"];
string serviceNamespace() => environment["SERVICE_NAMESPACE"];

void main(string[] args) => [
    "group": [
        "default" : [ "targets": ["emitter", "listener", "seed"] ].J,
        "cache" : [ "targets": ["runner-cache", "dependencies-cache"] ].J,
    ],
    "target": [
        "emitter": makeTarget("emitter"),
        "listener": makeTarget("listener"),
        "seed": makeTarget("seed"),
        "runner-cache": makeTarget!false("runner-cache"),
        "dependencies-cache": makeTarget!false("dependencies-cache")
    ]
].J.toPrettyString(JSONOptions.doNotEscapeSlashes).writeln;

J makeTarget(bool isAppImage = true)(string targetName)
{
    const depsCacheUrl = makeCacheUrl("dependencies", depsTag);
    const tag = isAppImage ? .tag : .depsTag;
    const imageTag = "%s/%s:%s".fmt(
        registryUrl,
        targetName,
        tag
    );
    const cacheUrl = [ makeCacheUrl(targetName, depsTag, isAppImage) ];

    return [
        "args": J(isAppImage ? [ "APP_NAME": targetName] : null),
        "cache-from": J((isAppImage? [depsCacheUrl] : []) ~ cacheUrl),
        "cache-to": cacheUrl.J,
        "context": ".".J,
        "dockerfile": "Dockerfile".J,
        "labels": [ "name": "%s-%s".fmt(serviceNamespace, targetName) ].J,
        "output": [ local ? "type=docker" : "type=registry" ].J,
        "platforms": [ "linux/amd64" ].J,
        "tags": [ imageTag ].J,
        "target": J(isAppImage ? "runner" : targetName),
    ].J;
}

string makeCacheUrl(string targetName, string imageTag, bool cache = true) =>
    "type=registry,ref=%s/%s%s:%s"
        .fmt(registryUrl, targetName, cache ? "-cache" : "", imageTag);
