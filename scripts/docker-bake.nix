{ tag ? "latest"
 ,depsTag ? "dev"
 ,registryUrl
 ,serviceNamespace ? "service"
}:
let
  result = builtins.toJSON {
    group = { default = { targets = ["emitter" "listener" "seed"]; }; };
    target = {
      seed = makeTarget { targetName = "seed"; };
      listener = makeTarget { targetName = "listener"; };
      emitter = makeTarget { targetName = "emitter"; };
      dependencies = makeTarget { targetName = "dependencies"; useDepsCache = false; };
    };
  };

  makeTarget = { targetName, local ? false, useDepsCache ? true }: {
    cache-from = [
      (makeCacheUrl { inherit targetName; } )
    ] ++ (if useDepsCache
      then [ (makeCacheUrl { targetName = "dependencies"; urlTag = depsTag; }) ]
      else []
    );
    cache-to = [ (makeCacheUrl { inherit targetName; }) ];
    context = ".";
    dockerfile = "Dockerfile";
    labels = { name = "${serviceNamespace}-${targetName}"; };
    output = [ "type=${if local then "Docker" else "registry"}" ];
    platforms = [ "linux/amd64" ];
    tags = [ "${registryUrl}/${targetName}:latest" ];
    target = targetName;
  };

  makeCacheUrl = { targetName, urlTag? tag }:
    "type=registry,ref=${registryUrl}/${targetName}-cache:${urlTag}";
in
result
