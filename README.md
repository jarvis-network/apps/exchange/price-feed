# Price Feed

To run the listener use this command:

```
dub run :listener
```

To run the emitter use this command:

```
dub run :emitter
```

To run the seed script use this command:

```
dub run :seed
```

## Emitter API

### `/subscribe`

At this WebSocket endpoint you can subcribe to price updates. And while subscribing you can also request historic prices for the same pairs.

#### Query parameters

| Name    | Required | Example         | Description                                                                                                                                                                                                                                                  |
| ------- | -------- | --------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `pairs` | ❌       | `EURUSD,GBPUSD` | The pairs to receive price updates for separated by comma. You will also receive the OHLC for today for every pair. If it's the weekend open, high, low, and close are going to be the same. Supported pairs are `EURUSD`, `GBPUSD`, `USDCHF`, and `XAUUSD`. |
| `from`  | ❌       | `2020-12-02`    | The date from which you want to receive historic prices for the `pairs` (inclusive). The date must be in `yyyy-MM-dd` format. If `to` is not provided no historic prices will be received.                                                                   |
| `to`    | ❌       | `2020-12-05`    | The date until which you want to receive historic prices for the `pairs` (inclusive). The date must be in `yyyy-MM-dd` format. If `from` is not provided no historic prices will be received.`                                                               |

#### Requests

##### `subscribe`

Allows you to subscribe to a pair without reconnecting.

###### Parameters

| Name   | Required | Example      | Description                                                                                                                                                                                                                             |
| ------ | -------- | ------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `pair` | ✅       | `EURUSD`     | The pair to receive price updates for. You will also receive the OHLC for today for that pair. If it's the weekend open, high, low, and close are going to be the same. Supported pairs are `EURUSD`, `GBPUSD`, `USDCHF`, and `XAUUSD`. |
| `from` | ❌       | `2020-12-02` | The date from which you want to receive historic prices for the `pairs` (inclusive). The date must be in `yyyy-MM-dd` format. If `to` is not provided no historic prices will be received.                                              |
| `to`   | ❌       | `2020-12-05` | The date until which you want to receive historic prices for the `pairs` (inclusive). The date must be in `yyyy-MM-dd` format. If `from` is not provided no historic prices will be received.`                                          |

###### Examples

```json
{
    "type": "subscribe",
    "pair": "EURUSD"
}
```

```json
{
    "type": "subscribe",
    "pairs": "GBPUSD",
    "from": "2020-12-02",
    "to": "2020-12-04"
}
```

##### `unsubscribe`

Allows you to unsubscribe from receiving updates for a pair without disconnecting.

###### Parameters

| Name   | Required | Example  | Description                                               |
| ------ | -------- | -------- | --------------------------------------------------------- |
| `pair` | ✅       | `EURUSD` | The pair to unsubscribe from receiving price updates for. |

###### Examples

```json
{
    "type": "unsubscribe",
    "pairs": "EURUSD,XAUUSD"
}
```

#### Messages

##### Price updates

Price updates have a `t` property representing a timestamp in seconds of type `number` and the updated price of type `number` in a key named after the pair. Received when after passing a valid `pairs` query parameter or using the [`subscribe`](#subscribe-1) request.

###### Examples

```json
{
    "t": 1607623614,
    "XAUUSD": 1831.96
}
```

##### Historic prices

An object containing an array of OHLC tuples for every requested pair and a `t` array containing the requested dates.

###### Examples

When `from` is `2020-11-30` and `to` is `2020-12-07`:

```json
{
    "EURUSD": [
        [1.1961, 1.2003, 1.1922, 1.1926],
        [1.1928, 1.2078, 1.1926, 1.1939],
        [1.2069, 1.2121, 1.2029, 1.2117],
        [1.2114, 1.2176, 1.2099, 1.2148],
        [1.2142, 1.2179, 1.2109, 1.2123],
        [1.2123, 1.2123, 1.2123, 1.2123],
        [1.2123, 1.2123, 1.2123, 1.2123],
        [1.2133, 1.2167, 1.2078, 1.2109]
    ],
    "GBPUSD": [
        [1.3332, 1.3385, 1.3302, 1.3325],
        [1.3327, 1.3444, 1.3315, 1.3419],
        [1.3419, 1.3443, 1.3287, 1.3367],
        [1.3373, 1.3501, 1.3352, 1.3454],
        [1.3451, 1.3541, 1.3409, 1.3442],
        [1.3442, 1.3442, 1.3442, 1.3442],
        [1.3442, 1.3442, 1.3442, 1.3442],
        [1.3413, 1.3439, 1.3223, 1.3383]
    ],
    "t": [
        "2020-11-30",
        "2020-12-01",
        "2020-12-02",
        "2020-12-03",
        "2020-12-04",
        "2020-12-05",
        "2020-12-06",
        "2020-12-07"
    ]
}
```

### `/price`

Request the price for a pair at a certain timestamp

#### Query parameters

| Name      | Required | Example      | Description                                                                                        |
| --------- | -------- | ------------ | -------------------------------------------------------------------------------------------------- |
| pair      | ✅       | `EURUSD`     | The pairs to receive the price of. Supported pairs are `EURUSD`, `GBPUSD`, `USDCHF`, and `XAUUSD`. |
| timestamp | ✅       | `1607623614` | The timestamp in secondsrepresenting the second at which the price was requested                   |

#### Response

##### Values

| Name  | Description                                                  |
| ----- | ------------------------------------------------------------ |
| price | The requested price                                          |
| t     | The actual timestamp in seconds at which the update happened |

##### Example

```json
{
    "price": 1.34158,
    "t": 1608604454
}
```

## Deployment

1. Use `docker login registry.gitlab.com` to login to the GitLab Container Registry
2. Set the `TAG` environment variable to the current commit hash (or
   alternatively `latest` to deploy the latest build of the `dev` branch)
3. (Optionally) Build the images with `docker buildx bake` (which will publish
   to GitLab CI when the building succeeds) if you want to deploy a commit
   which is not yet build on GitLab
4. Make a new Helm release which will deploy the image with tag `TAG`

* Fish
  ```sh
  set -x TAG (git rev-parse HEAD)
  docker buildx bake listener emitter
  helm upgrade --install emitter ./helm/emitter --values=./helm/emitter/values.yaml --set "image.tag=$TAG" -n price-bot-dev
  helm upgrade --install listener ./helm/listener --values=./helm/listener/values.yaml --set "image.tag=$TAG" -n price-bot-dev
  ```
* Bash
  ```bash
  export TAG="$(git rev-parse HEAD)"
  docker buildx bake listener emitter
  helm upgrade --install emitter ./helm/emitter --values=./helm/emitter/values.yaml --set image.tag=${TAG} -n price-bot-dev
  helm upgrade --install listener ./helm/listener --values=./helm/listener/values.yaml --set image.tag=${TAG} -n price-bot-dev
  ```
