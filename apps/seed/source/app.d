module app;

private struct OHLC
{
    string date;
    double open;
    double high;
    double low;
    double close;
}

private struct TimeseriesResponse
{
    OHLC[] quotes;
}

private struct HourlyOHLCResponse
{
    string currency;
    string date_time;
    double open;
    double high;
    double low;
    double close;
}

private auto parseTimeseries(string text)
{
    try
    {
        import asdf : deserialize;

        return text.deserialize!TimeseriesResponse.quotes;
    }
    catch (Exception exception)
    {
        import std.stdio : writeln;

        writeln("Failed to parse timeseries");
        writeln("text: ", text);
        writeln("message: ", exception.msg);

        throw exception;
    }
}

private auto parseHourly(string text)
{
    try
    {
        import asdf : deserialize;

        return text.deserialize!HourlyOHLCResponse;
    }
    catch (Exception exception)
    {
        import std.stdio : writeln;

        writeln("Failed to parse hourly");
        writeln("text: ", text);
        writeln("message: ", exception.msg);

        throw exception;
    }
}

void main()
{
    import core.time : days;
    import std.datetime.systime : Clock, SysTime;
    import std.datetime.date : Date, DateTime, DayOfWeek;
    import std.datetime.timezone : PosixTimeZone;
    import std.process : environment;
    import std.stdio : writeln, writefln;
    import influxdb : Database, Measurement, InfluxValue;
    import price_feed.models.pairs : stringPairs;

    const database = Database(environment["INFLUXDB_URL"], environment["INFLUXDB_DB_NAME"]);

    const today = cast(Date) Clock.currTime;
    const fourMonthsAgo = today - 120.days;

    const tz = PosixTimeZone.getTimeZone("Europe/London");

    writeln("Starting price-feed db seed task");

    foreach (pair; stringPairs)
    {
        import vibe.http.client : requestHTTP;
        import vibe.stream.operations : readAllUTF8;

        const url = `https://marketdata.tradermade.com/api/v1/timeseries?start_date=`
            ~ fourMonthsAgo.toISOExtString ~ `&end_date=` ~ today.toISOExtString
            ~ `&api_key=` ~ environment["TRADERMADE_REST_API_KEY"] ~ `&currency=` ~ pair;

        auto result = requestHTTP(url).bodyReader.readAllUTF8.parseTimeseries;

        foreach (i, quote; result)
        {
            writefln("[%s/%s]: Inserting '%s' price point for: %s", i, result.length, pair, quote);

            // Skip if weekend
            const date = Date.fromISOExtString(quote.date);
            if (date.dayOfWeek == DayOfWeek.sat || date.dayOfWeek == DayOfWeek.sun)
            {
                continue;
            }

            database.insert(Measurement(pair, ["price": InfluxValue(quote.open)],
                    SysTime(DateTime.fromISOExtString(quote.date ~ "T08:30:00"), tz)));

            // FIXME: mapping from OHLC to price:
            database.insert(Measurement(pair, ["price": InfluxValue(quote.low)],
                    SysTime(DateTime.fromISOExtString(quote.date ~ "T09:00:00"), tz)));

            // FIXME: mapping from OHLC to price:
            database.insert(Measurement(pair, ["price": InfluxValue(quote.high)],
                    SysTime(DateTime.fromISOExtString(quote.date ~ "T15:00:00"), tz)));

            database.insert(Measurement(pair, [
                        "price": InfluxValue(quote.close)
                    ], SysTime(DateTime.fromISOExtString(quote.date ~ "T15:30:00"), tz)));
        }

        foreach (offset; 0 .. 120)
        {
            const date = fourMonthsAgo + offset.days;

            if (date.dayOfWeek != DayOfWeek.sun)
            {
                continue;
            }

            const dateString = date.toISOExtString;

            const sundayFirstRequestURL = `https://marketdata.tradermade.com/api/v1/hour_historical?currency=` ~ pair
                ~ `&date_time=` ~ date.toISOExtString ~ `-22:00&api_key=`
                ~ environment["TRADERMADE_REST_API_KEY"];
            auto firstResponse = requestHTTP(sundayFirstRequestURL).bodyReader
                .readAllUTF8.parseHourly;
            database.insert(Measurement(pair, [
                        "price": InfluxValue(firstResponse.open)
                    ], SysTime(DateTime.fromISOExtString(dateString ~ "T22:05:00"), tz)));

            database.insert(Measurement(pair, [
                        "price": InfluxValue(firstResponse.low)
                    ], SysTime(DateTime.fromISOExtString(dateString ~ "T22:10:00"), tz)));

            database.insert(Measurement(pair, [
                        "price": InfluxValue(firstResponse.high)
                    ], SysTime(DateTime.fromISOExtString(dateString ~ "T22:20:00"), tz)));

            database.insert(Measurement(pair, [
                        "price": InfluxValue(firstResponse.close)
                    ], SysTime(DateTime.fromISOExtString(dateString ~ "T22:55:00"), tz)));

            const sundaySecondRequestURL = `https://marketdata.tradermade.com/api/v1/hour_historical?currency=` ~ pair
                ~ `&date_time=` ~ date.toISOExtString ~ `-23:00&api_key=`
                ~ environment["TRADERMADE_REST_API_KEY"];
            auto secondResponse = requestHTTP(sundaySecondRequestURL)
                .bodyReader.readAllUTF8.parseHourly;
            database.insert(Measurement(pair, [
                        "price": InfluxValue(secondResponse.open)
                    ], SysTime(DateTime.fromISOExtString(dateString ~ "T23:05:00"), tz)));

            database.insert(Measurement(pair, [
                        "price": InfluxValue(secondResponse.low)
                    ], SysTime(DateTime.fromISOExtString(dateString ~ "T23:10:00"), tz)));

            database.insert(Measurement(pair, [
                        "price": InfluxValue(secondResponse.high)
                    ], SysTime(DateTime.fromISOExtString(dateString ~ "T23:20:00"), tz)));

            database.insert(Measurement(pair, [
                        "price": InfluxValue(secondResponse.close)
                    ], SysTime(DateTime.fromISOExtString(dateString ~ "T23:55:00"), tz)));
        }
    }

    writeln("price-feed completed successfully");
}
