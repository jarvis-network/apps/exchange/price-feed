module models.pricepoint;

/// Parsed result
struct PricePoint
{
    import std.datetime : SysTime;
    import price_feed.models.pairs : Pair;

    /// Bid price
    double bid;
    /// Ask price
    double ask;
    /// Mid price
    double mid;
    /// Timestamp
    SysTime time;
    /// The pair
    Pair pair;
}
