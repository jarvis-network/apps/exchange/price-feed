import dataproviders.interfaces : DataProvider;
import database.interfaces : Database;

void main()
{
    import dataproviders.tradermade : TraderMade;
    import database.influxdb : InfluxDB;
    import std.process : environment;

    logInfo("Listener started");

    run(
        new TraderMade(environment["TRADERMADE_STREAMING_API_KEY"]),
        new InfluxDB(environment["INFLUXDB_URL"], environment["INFLUXDB_DB_NAME"])
    );
}

private void logInfo(string message)
{
    import std : writeln;

    writeln(message);

}

private void logError(string message)
{
    import std.stdio : stderr;

    stderr.writeln(message);
}

/// Actual main function
void run(DataProvider dataProvider, Database database)
{
    import price_feed.models.pairs : Pair, enumPairs;
    import optional : none;

    dataProvider.connect(enumPairs);

    foreach (message; dataProvider)
    {
        const pricePoint = dataProvider.parseData(message);
        if (pricePoint != none)
        {
            database.insert(pricePoint.front);
        }
    }

    logInfo("Disconnected");
    // TODO: Reconnect
}
