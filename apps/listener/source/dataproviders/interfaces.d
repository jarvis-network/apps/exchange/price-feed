module dataproviders.interfaces;

/// A common interface for every data provider
interface DataProvider
{
    import price_feed.models.pairs : Pair;
    import optional : Optional;
    import models.pricepoint : PricePoint;

    /// Parses a string received from the provider
    Optional!(const PricePoint) parseData(string data);

    /// Sends a message to the provider to subscribe to chosen pairs
    void connect(const Pair[] pairs);

    /// https://tour.dlang.org/tour/en/gems/opdispatch-opapply
    int opApply(int delegate(string) dg);
}
