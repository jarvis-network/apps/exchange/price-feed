module dataproviders.testprovider;

/// Used for testing
class TestProvider : dataproviders.interfaces.DataProvider
{
    import price_feed.models.pairs : Pair, enumPairs;
    import optional : Optional;
    import models.pricepoint : PricePoint;

    /// Parses a string received from the provider
    Optional!(const PricePoint) parseData(string data)
    {
        return typeof(return).init;
    }

    /// Sends a message to the provider to subscribe to chosen pairs
    void connect(const Pair[] pairs)
    {

    }

    /// https://tour.dlang.org/tour/en/gems/opdispatch-opapply
    int opApply(int delegate(string) dg)
    {
        import std.conv : to;

        foreach (i; 0 .. 5)
        {
            if (auto result = dg(i.to!string))
            {
                return result;
            }
        }
        return 0;
    }

    unittest
    {
        TestProvider provider = new TestProvider();
        provider.connect(pairs);

        string[] results = [];
        foreach (message; provider)
        {
            results ~= message;
        }

        assert(results == ["0", "1", "2", "3", "4"]);
    }
}
