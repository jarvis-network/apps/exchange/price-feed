module dataproviders.tradermade;

debug import std.stdio : writeln;

/// TraderMade data provider
class TraderMade : dataproviders.interfaces.DataProvider
{
    import vibe.http.websockets : WebSocket;
    import price_feed.models.pairs : Pair;
    import optional : Optional, no, some;
    import models.pricepoint : PricePoint;

    /// Message from TraderMade about a price update
    private struct PriceUpdateMessage
    {
        /// The pair
        string pair;
        /// Bid price
        double bid;
        /// Ask price
        double ask;
        /// Mid price
        double mid;
        /// Timestamp
        string time;
    }

    private
    {
        WebSocket webSocket;
        string apiKey;
    }

    /// TraderMade data provider constructor
    this(string apiKey)
    {
        this.apiKey = apiKey;
    }

    /// Parses a string received from TraderMade
    Optional!(const PricePoint) parseData(string data) @safe
    {
        import std.regex : ctRegex, matchFirst;
        import std.conv : to;
        import std.datetime.systime : SysTime;
        import std.csv : csvReader;

        if (data == "Connected")
        {
            return no!(const PricePoint);
        }

        try
        {
            const message = csvReader!PriceUpdateMessage(data, ' ').front;

            // Time format is YYYYMMDD-HH:mm:ss.SSS
            const time = SysTime.fromISOExtString(
                    message.time[0 .. 4] ~ '-' ~ message.time[4 .. 6] ~ '-'
                    ~ message.time[6 .. 8] ~ 'T' ~ message.time[9 .. 21]);
            const PricePoint result = PricePoint(message.bid, message.ask,
                    message.mid, time, message.pair.to!Pair);

            debug (TraderMadeMessage)
                result.writeln;
            return result.some;
        }
        catch (Exception e)
        {
            debug writeln("Error parsing data = '", data, "'. Exception:", e);
            return no!(const PricePoint);
        }
    }

    /// Sends a message to TraderMade to subscribe to chosen pairs
    void connect(const Pair[] pairs) @safe
    in(this.webSocket is null, "Connect has already been called")
    {
        import std.array : join;
        import std.conv : to;
        import std.algorithm.iteration : map;
        import vibe.http.websockets : connectWebSocket;
        import vibe.inet.url : URL, PosixPath;

        const url = URL("wss", "marketdata.tradermade.com", 443, PosixPath("/feed"));
        this.webSocket = connectWebSocket(url);

        const symbol = pairs.map!(pair => to!string(pair)).join(',');
        webSocket.send("{\"userKey\":\"" ~ this.apiKey ~ "\", \"symbol\":\"" ~ symbol ~ "\"}");
    }

    /// https://tour.dlang.org/tour/en/gems/opdispatch-opapply
    int opApply(int delegate(string) dg)
    {
        // TODO: Check if vibe is automatically doing ping
        while (webSocket.waitForData)
        {
            const text = webSocket.receiveText;
            if (auto result = dg(text))
            {
                return result;
            }
        }
        return 0;
    }
}
