module database.influxdb;

/// Helper class to talk to InfluxDB
class InfluxDB : database.interfaces.Database
{
    import influxdb : Database;
    import models.pricepoint : PricePoint;

    private
    {
        Database database;
    }

    /// Connects to InfluxDB
    this(string url, string name)
    {
        this.database = Database(url, name);
    }

    /// Insert a price point into database
    void insert(PricePoint data)
    {
        import std.conv : to;
        import influxdb : Measurement, InfluxValue;

        this.database.insert(Measurement(data.pair.to!string,
                ["price": InfluxValue(data.mid),], data.time));
    }
}
