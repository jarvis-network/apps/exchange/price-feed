module database.interfaces;

/// Common database interface
interface Database
{
    import models.pricepoint : PricePoint;

    /// Insert a price point into database
    void insert(PricePoint data);
}
