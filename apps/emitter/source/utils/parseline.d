module utils.parseline;

import models.update : Update;
import std.range.primitives : isInputRange;

private auto find(alias pred = "a == b", R)(R haystack)
{
    import std.algorithm.searching : countUntil;

    const index = haystack.countUntil!(pred);
    assert(index != -1);
    return haystack[index];
}

/**
Parses a line.

https://docs.influxdata.com/influxdb/v1.8/write_protocols/line_protocol_tutorial/
*/
Update* parseLine(const string line) pure
{
    import std.string : split;
    import std.conv : to;
    import std.algorithm.searching : startsWith;
    import std.typecons : No;
    import std.array : appender;
    import asdf : jsonSerializer;
    import price_feed.models.pairs : Pair;

    const parts = line.split('\n')[0].split(' ');
    if (parts.length != 3)
    {
        throw new Exception("line doesn't contain 3 sections splitted by a space");
    }

    const pair = parts[0].split(',')[0];
    const price = parts[1].split(',').find!(item => item.startsWith("price="))[6 .. $].to!double;
    const timestamp = parts[2].to!ulong;

    auto app = appender!string;
    auto serializer = jsonSerializer(&app.put!(const(char)[]));
    auto object = serializer.objectBegin;

    serializer.putEscapedKey("t");
    serializer.putValue(timestamp / 1_000_000_000);

    serializer.putEscapedKey(pair);
    serializer.putValue(price);

    serializer.objectEnd(object);
    serializer.flush;

    return new Update(pair.to!Pair, app.data, price, timestamp);
}
