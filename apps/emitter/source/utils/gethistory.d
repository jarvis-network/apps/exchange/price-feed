module utils.gethistory;

import std.array : join;
import std.algorithm.iteration : map;
import std.conv : text;
import std.datetime.date : DateTime;
import std.exception : enforce;

import influxdb : Database, Result;
import price_feed.models.pairs : Pair;

version (unittest)
{
    import std.exception : collectExceptionMsg;
    import std.datetime.systime : Clock;
}

Result getLatestPrice(Database database, const Pair[] pairs, DateTime timestamp)
{
    import std.format : format;

    const select = `SELECT LAST(price)`;
    const from = format(`FROM %-("` ~ database.db ~ `"."autogen"."%s"%|,%)`, pairs);
    const where = format(`WHERE time < '%sZ'`, timestamp.toISOExtString);
    const fill = `fill(none)`;
    const query = [select, from, where, fill].join("\n    ");

    return database.query(query).results[0];
}

enum Interval
{
    second = "s",
    minute = "m",
    hourly = "h",
    daily = "d",
}

Result getHistoryEntries(
    Database database,
    const Pair[] pairs,
    DateTime startTime,
    DateTime endTime,
    Interval interval = Interval.daily,
    uint intervalLength = 1)
{
    import std.datetime.systime : Clock;
    import std.format : format;
    import core.time : days;

    enforce(startTime < endTime, "`startTime` bigger than endTime");
    enforce(endTime < cast(DateTime) Clock.currTime, "`endTime` bigger than today");

    enum select = `SELECT
        FIRST(price) as open,
        MAX(price) as high,
        MIN(price) as low,
        LAST(price) as close`;
    const from = format(`FROM %-("` ~ database.db ~ `"."autogen"."%s"%|,%)`, pairs);
    const where = format!`WHERE time >= '%sZ' AND time < '%sZ'`(
        startTime.toISOExtString,
        endTime.toISOExtString
    );
    const groupBy = format!`GROUP BY time(%s%s)`(intervalLength, cast(string)interval);
    const fill = `fill(none)`;
    const query = [select, from, where, groupBy, fill].join("\n    ");

    // TODO: Cache
    return database.query(query).results[0];
}

string getHistory(Database database, const Pair[] pairs, DateTime startTime, DateTime endTime)
{
    import std.array : appender;
    import asdf : jsonSerializer;
    import core.time : days;
    import std.datetime.date : DayOfWeek;
    import std.datetime.systime : Clock;

    if (pairs.length == 0)
    {
        return "";
    }

    const today = cast(DateTime) Clock.currTime;
    if (endTime > today)
    {
        endTime = today;
    }

    auto app = appender!string;
    auto serializer = jsonSerializer(&app.put!(const(char)[]));
    auto object = serializer.objectBegin();

    const queryFromDate = startTime.dayOfWeek == DayOfWeek.sun ? startTime - 2.days
        : startTime.dayOfWeek == DayOfWeek.sat ? startTime - 1.days : startTime;
    const queryFromDateISOExtString = queryFromDate.toISOExtString ~ "T00:00:00Z";
    const queryFromDateISOExtString2 = (startTime.dayOfWeek == DayOfWeek.sun
            ? (startTime - 1.days).toISOExtString : queryFromDateISOExtString) ~ "T00:00:00Z";

    auto response = getHistoryEntries(database, pairs, queryFromDate, endTime);

    foreach (series; response.series)
    {
        serializer.putEscapedKey(series.name);
        auto seriesArray = serializer.arrayBegin();

        auto yesterday = series.values[0];

        foreach (row; series.values)
        {
            import std.conv : to;

            if (queryFromDate != startTime && (queryFromDateISOExtString == row[0]
                    || queryFromDateISOExtString2 == row[0]))
            {
                continue;
            }

            serializer.elemBegin();

            // Checking if weekend
            if (row[1].length == 0)
            {
                const close = yesterday[4].to!double;
                auto ohlc = serializer.arrayBegin();
                serializer.elemBegin();
                serializer.putValue(close);
                serializer.elemBegin();
                serializer.putValue(close);
                serializer.elemBegin();
                serializer.putValue(close);
                serializer.elemBegin();
                serializer.putValue(close);
                serializer.arrayEnd(ohlc);
                continue;
            }

            debug
            {
                assert(row[2].length != 0);
                assert(row[3].length != 0);
                assert(row[4].length != 0);
            }

            auto ohlc = serializer.arrayBegin();

            const open = row[1].to!double;
            serializer.elemBegin();
            serializer.putValue(open);
            const high = row[2].to!double;
            serializer.elemBegin();
            serializer.putValue(high);
            const low = row[3].to!double;
            serializer.elemBegin();
            serializer.putValue(low);
            const close = row[4].to!double;
            serializer.elemBegin();
            serializer.putValue(close);

            serializer.arrayEnd(ohlc);

            debug
            {
                assert(low <= high);
                assert(low <= open);
                assert(low <= close);

                assert(high >= open);
                assert(high >= close);
            }

            yesterday = row;
        }

        serializer.arrayEnd(seriesArray);
    }

    serializer.putEscapedKey("t");
    auto daysArray = serializer.arrayBegin();

    foreach (index; 0 .. (endTime + 1.days - startTime).total!"days")
    {
        serializer.elemBegin();
        serializer.putValue((startTime + index.days).toISOExtString);
    }

    serializer.arrayEnd(daysArray);
    serializer.objectEnd(object);
    serializer.flush;

    return app.data;
}

@("throws when from date is bigger than to date")
unittest
{
    assert(
        collectExceptionMsg(
            getHistory(Database.init, [Pair.EURUSD], DateTime(2000, 12, 31), DateTime(2000, 01, 01))
        ) == "`startTime` bigger than endTime"
    );
}

@("throws when end date is bigger than today")
unittest
{
    import core.time : days;

    auto now = cast(DateTime) Clock.currTime;
    auto yesterday = now - 1.days;
    auto tomorrow = now + 1.days;

    assert(
        collectExceptionMsg(
            getHistory(Database.init, [Pair.EURUSD], yesterday, tomorrow)
        ) == "`endTime` bigger than today"
    );
}

string getHistory(Database database, const Pair[] pairs, const string startTime, const string endTime)
{
    return getHistory(database, pairs, DateTime.fromISOExtString(startTime),
            DateTime.fromISOExtString(endTime));
}
