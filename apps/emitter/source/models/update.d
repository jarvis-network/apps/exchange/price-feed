module models.update;

/// A price update model
const struct Update
{
    import price_feed.models.pairs : Pair;

    /// The pair which got updated
    Pair pair;
    /// The message
    string message;
    /// The new price
    double price;
    /// Nanoseconds since 1970
    ulong timestamp;
}
