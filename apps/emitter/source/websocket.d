module websocket;

debug import std.stdio : writeln;

/// Handles WebSocket connections from client applications
class WebsocketService
{
    import vibe.http.websockets : WebSocket;
    import vibe.web.common : path, queryParam;
    import state : State;
    import price_feed.models.pairs : Pair;
    import influxdb : Database;
    import vibe.http.server : HTTPServerRequest;

    private
    {
        Database database;
        State state;
    }

    /// Constructor
    this(Database database, State state)
    {
        this.database = database;
        this.state = state;
    }

    @path("/subscribe")
    void getSubscribe(scope WebSocket socket, scope HTTPServerRequest request)
    {
        import std.conv : convertTo = to;
        import std.string : split;
        import std.datetime.systime : Clock;
        import std.datetime.date : Date;
        import vibe.core.core : runTask;
        import utils.gethistory : getHistory;

        auto pairsArray = request.query.get("pairs", "").split(',').convertTo!(Pair[]);

        const from = request.query.get("from", "");
        const to = request.query.get("to", "");

        void sendHistoricPrices(const Pair[] pairs, const string from, const string to)
        {
            runTask({
                const history = getHistory(database, pairs, from, to);
                history.length && socket.send(history);
            });
        }

        void sendTodayPrices(const Pair[] pairs)
        {
            const todayString = (cast(Date) Clock.currTime).toISOExtString;
            sendHistoricPrices(pairs, todayString, todayString);
        }

        void sendHistoricPricesOnSubscribe(const Pair[] pairs, const string from, const string to)
        {
            if (from.length && to.length)
            {
                // TODO: Check format
                import std.algorithm.comparison : cmp;

                const today = (cast(Date) Clock.currTime).toISOExtString;
                if (today.cmp(to) <= 0)
                {
                    sendHistoricPrices(pairs, from, today);
                }
                else
                {
                    sendHistoricPrices(pairs, from, to);
                    sendTodayPrices(pairs);
                }
            }
            else
            {
                sendTodayPrices(pairs);
            }
        }

        if (pairsArray.length)
        {
            sendHistoricPricesOnSubscribe(pairsArray, from, to);
        }

        debug writeln("Got new web socket connection with pairs ", pairsArray);

        auto channelWithId = state.createChannelWithId(); // @suppress(dscanner.suspicious.unmodified)
        auto channel = channelWithId.channel;
        auto channelId = channelWithId.id;
        scope (exit)
        {
            state.deleteChannel(channelId);
        }

        runTask({
            import std.algorithm.searching : find;
            import std.range : empty;
            import std.conv : to;
            import models.update : Update;
            import price_feed.models.pairs : Pair, enumPairs;

            struct LastUpdate
            {
                double price;
                ulong timestamp;
            }

            LastUpdate[Pair] lastUpdates;
            foreach (pair; enumPairs)
                lastUpdates[pair] = LastUpdate(0, 0);

            shared Update* item;
            while (channel.tryConsumeOne(item))
            {
                if (!socket.connected)
                {
                    break;
                }

                if (pairsArray.find(item.pair.to!string).empty)
                {
                    continue;
                }

                auto lastUpdate = lastUpdates[item.pair];
                // Can be moved before state.push but there will be a race condition; Do it if emitter takes too much memory
                if (lastUpdate.price == item.price || lastUpdate.timestamp > item.timestamp)
                {
                    continue;
                }

                socket.send(item.message);

                lastUpdates[item.pair] = LastUpdate(item.price, item.timestamp);
            }
        });

        while (socket.waitForData)
        {
            const serializedMessage = socket.receiveText();
            if (serializedMessage.length)
            {
                import std.json : parseJSON, JSONValue;
                import std.algorithm.mutation : remove;
                import std.algorithm.searching : countUntil;

                JSONValue message = parseJSON(serializedMessage);
                if (message["type"].str == "subscribe")
                {
                    import price_feed.models.pairs : stringPairs;
                    import std.algorithm.searching : canFind;
                    import std.range : empty;

                    const pair = message["pair"].str;
                    if (!stringPairs.canFind(pair))
                    {
                        continue;
                    }

                    pairsArray ~= pair.convertTo!Pair;

                    const subscribeFrom = "from" in message ? message["from"].str : "";
                    const subscribeTo = "to" in message ? message["to"].str : "";

                    sendHistoricPricesOnSubscribe([pair.convertTo!Pair], subscribeFrom, subscribeTo);
                }
                else if (message["type"].str == "unsubscribe")
                {
                    pairsArray = pairsArray.remove!(p => p == message["pair"].str.convertTo!Pair);
                }
            }
        }

        debug writeln("Client disconnected.");
    }
}
