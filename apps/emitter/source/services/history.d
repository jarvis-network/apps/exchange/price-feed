module services.history;

import std.array : array;
import std.algorithm : canFind, map;
import std.conv : to;
import std.datetime : DateTime, SysTime;
import std.exception : enforce;
debug import std.stdio;
import influxdb.api;
import vibe.data.serialization : name;


import price_feed.models.pairs : Pair;
import utils.gethistory : Interval, getLatestPrice, getHistoryEntries;

struct TimeSeriesResult
{
    @name("base_currency")
    string baseCurrency;

    @name("quote_currency")
    string quoteCurrency;

    @name("start_date")
    SysTime startDate;

    @name("end_date")
    SysTime endDate;

    OhlcPoint[] quotes;

    struct OhlcPoint
    {
        SysTime date;
        double open;
        double high;
        double low;
        double close;
    }
}

struct LiveResult
{
    SysTime timestamp;
    LivePricePoint[] quotes;

    struct LivePricePoint
    {
        double bid;
        double ask;
        double mid;
        string instrument;
    }
}

interface IHistoryService
{
    TimeSeriesResult getTimeseries(Pair currency, string start_date, string end_date, string interval, uint period = 1);
    TimeSeriesResult getTimeseries(Pair currency, DateTime start_date, DateTime end_date, string interval, uint period = 1);
    LiveResult getLive(Pair currency);
}

class HistoryService : IHistoryService
{
    private
    {
        import influxdb : Database;
        Database database;
    }

    /// Constructor
    this(Database database)
    {
        this.database = database;
    }

    TimeSeriesResult getTimeseries(Pair currency, string startDate, string endDate, string interval, uint period = 1)
    {
        import std.datetime : Date, TimeOfDay;
        const start = DateTime(
            Date.fromISOExtString(startDate[0 .. 10]),
            TimeOfDay(startDate[11 .. 13].to!int, startDate[14 .. 16].to!int),
        );
        const end = DateTime(
            Date.fromISOExtString(endDate[0 .. 10]),
            TimeOfDay(endDate[11 .. 13].to!int, endDate[14 .. 16].to!int),
        );
        return this.getTimeseries(currency, start, end, interval, period);
    }

    TimeSeriesResult getTimeseries(Pair currency, DateTime startDate, DateTime endDate, string interval, uint period = 1)
    {
        import std.datetime.timezone : UTC;

        const series = getHistoryEntries(database, [currency], startDate, endDate, interval.to!Interval, period)
            .series[0];

        TimeSeriesResult result =
        {
            baseCurrency: series.name[0 .. 3],
            quoteCurrency: series.name[3 .. $],
            startDate: SysTime(startDate, UTC()),
            endDate: SysTime(endDate, UTC()),
            quotes: series.values.map!(x => TimeSeriesResult.OhlcPoint(
                SysTime.fromISOExtString(x[0]),
                x[1].to!double,
                x[2].to!double,
                x[3].to!double,
                x[4].to!double,
            )).array
        };

        return result;
    }

    LiveResult getLive(Pair currency)
    {
        import std.datetime.systime : Clock;
        const timestamp = Clock.currTime;
        const series = getLatestPrice(database, [currency], cast(DateTime)timestamp.toUTC())
            .series[0];

        LiveResult result = {
            timestamp: SysTime.fromISOExtString(series.values[0][0]),
            quotes: series.values.map!(x => LiveResult.LivePricePoint(
                x[1].to!double, // bid = ask = mid = price
                x[1].to!double,
                x[1].to!double,
                series.name,
            )).array
        };

        return result;
    }
}
