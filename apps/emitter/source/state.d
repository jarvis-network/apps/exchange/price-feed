module state;

/// The state of the application
class State
{
    import vibe.core.channel : Channel;
    import models.update : Update;

    /// Helper return value for createChannelWithId
    struct ChannelWithId
    {
        /// The channel
        Channel!(shared Update*) channel;
        /// The id of the channel used for deleting
        string id;
    }

    private
    {
        Channel!(shared Update*)[string] channels;
    }

    /// Add a new update in the state
    void push(shared Update* update)
    {
        foreach (channel; channels)
        {
            channel.put(update);
        }
    }

    /// Creates a channel which will receive updates
    ChannelWithId createChannelWithId()
    {
        import std.uuid : randomUUID;
        import vibe.core.channel : createChannel;

        auto channel = createChannel!(shared Update*)();
        const id = randomUUID.toString;
        channels[id] = channel;
        return ChannelWithId(channel, id);
    }

    /// Prevents a channel from receiving more updates
    void deleteChannel(const string id)
    {
        auto channel = channels[id];
        channels.remove(id);
        channel.close();
    }
}
