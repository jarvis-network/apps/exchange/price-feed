module influxdbnotificationservice;

debug import std.stdio : writeln;

/// Subscribes and processes messages from InfluxDB
class InfluxDBNotificationService
{
    import vibe.web.common : path;
    import vibe.http.server : HTTPServerRequest, HTTPServerResponse;
    import state : State;
    import influxdb : Database;
    import vibe.core.core : Timer;

    private
    {
        Database database;
        State state;
        Timer timer;
    }

    /// Constructor
    this(Database database, State state)
    {
        this.database = database;
        this.state = state;
    }

    /// Subscribe to updates in InfluxDB
    void subscribe()
    {
        import vibe.core.core : setTimer;
        import std.process : environment;
        import mir.serde : SerdeException;
        import core.time : seconds;
        import price_feed.models.pairs : stringPairs;
        import std.format : format;
        import std.algorithm.iteration : map;
        import std.conv : to;
        import std.array : join;

        const from = stringPairs.map!(pair => `"%s"."autogen"."%s"`.format(database.db, pair)).join(',');
        const query = `SELECT LAST(price) as close FROM %s`.format(from);

        timer = setTimer(1.seconds, {
            import std.datetime.systime : SysTime;
            import std.array : appender;
            import asdf : jsonSerializer;
            import models.update : Update;
            import price_feed.models.pairs : Pair;

            auto result = database.query(query);
            foreach (series; result.results[0].series)
            {
                const pair = series.name;
                const values = series.values[0];
                const timestamp = SysTime.fromISOExtString(values[0]).toUnixTime;
                const price = values[1].to!double;

                auto app = appender!string;
                auto serializer = jsonSerializer(&app.put!(const(char)[]));
                auto object = serializer.objectBegin;

                serializer.putEscapedKey("t");
                serializer.putValue(timestamp);

                serializer.putEscapedKey(pair);
                serializer.putValue(price);

                serializer.objectEnd(object);
                serializer.flush;

                state.push(new Update(pair.to!Pair, app.data, price, timestamp));
            }
        }, true);
    }

    /// Unsubscribe to updates in InfluxDB
    void unsubscribe()
    {
        timer.stop();
    }
}
