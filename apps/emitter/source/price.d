module price;

debug import std.stdio : writeln;

/// The endpoint to get historical price for a certain timestamp
class PriceService
{
    import vibe.web.common : queryParam;
    import vibe.http.server : HTTPServerResponse;
    import influxdb : Database;

    private
    {
        Database database;
    }

    /// Constructor
    this(Database database)
    {
        this.database = database;
    }

    @queryParam("pair", "pair")
    @queryParam("timestamp", "timestamp")
    void getPrice(HTTPServerResponse response, string pair, ulong timestamp)
    {
        import std.conv : to;
        import std.datetime.systime : SysTime;
        import vibe.vibe : Json;
        import price_feed.models.pairs : stringPairs;
        import std.algorithm.searching : find;
        import std.range : empty;

        if (stringPairs.find(pair).empty)
        {
            import std.string : format;

            throw new Exception("Unsupported pair '%s'".format(pair));
        }

        try
        {
            const row = database.query(`SELECT LAST("price") FROM "db0"."autogen"."`
                    ~ pair ~ `" WHERE time < ` ~ timestamp.to!string ~ `000000000`)
                .results[0].series[0].rows[0];

            response.writeJsonBody([
                    "price": Json(row["last"].to!double),
                    "t": Json(SysTime.fromISOExtString(row["time"]).toUnixTime)
                    ]);
        }
        catch (Exception e)
        {
            if (e.msg == "ASDF deserialisation: non-optional member 'series' in Result is missing.")
            {
                response.writeJsonBody([
                        "price": Json(0),
                        "t": Json(timestamp.to!double)
                        ]);
            }
            else
            {
                throw e;
            }
        }
    }
}
