module app;

void main()
{
    import websocket : WebsocketService;
    import influxdbnotificationservice : InfluxDBNotificationService;
    import price : PriceService;
    import state : State;
    import services.history : HistoryService;
    import vibe.http.router : URLRouter;
    import vibe.inet.url : URL;
    import vibe.web.web : registerWebInterface;
    import vibe.web.rest : registerRestInterface, RestInterfaceSettings;
    import vibe.http.server : HTTPServerSettings, listenHTTP;
    import vibe.core.core : runApplication;
    import influxdb : Database;
    import std.process : environment;

    const database = Database(environment["INFLUXDB_URL"], environment["INFLUXDB_DB_NAME"]);

    auto router = new URLRouter;

    auto state = new State;

    auto notificationService = new InfluxDBNotificationService(database, state);
    router.registerWebInterface(new WebsocketService(database, state));
    router.registerWebInterface(new PriceService(database));

    auto restSettings = new RestInterfaceSettings;
    restSettings.baseURL = URL("/api");

    router.registerRestInterface(
        new HistoryService(database),
        restSettings
    );

    auto settings = new HTTPServerSettings;
    settings.port = 8080;
    settings.bindAddresses = ["::1", "0.0.0.0"];
    auto l = listenHTTP(settings, router);

    notificationService.subscribe();

    scope (exit)
    {
        l.stopListening();
        notificationService.unsubscribe();
    }
    runApplication();
}
