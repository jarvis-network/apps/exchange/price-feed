{ pkgs ? import <nixpkgs> { } }: with pkgs;
let
  dmd = pkgs.callPackage ./nix/pkgs/dmd-binary.nix {
    version = "2.098.0";
    hashes = {
      #   nix-prefetch-url http://downloads.dlang.org/releases/2.x/2.098.0/dmd.2.098.0.linux.tar.xz
      linux = "0i81vg218n3bbj2s2x6kl6xqdw3vajz74ynpk2vjhy6lkzjya10i";
      osx = "0hpkqwj80ydbmssch9a4gzknnrbmyw37g43ygrj9ljcx8baam03p";
    };
  };
  dub = pkgs.dub.overrideAttrs (oldAttrs: rec {
      version = "v1.27.0";
      src = fetchFromGitHub {
        owner = "dlang";
        repo = "dub";
        rev = "v${version}";
        sha256 = "1zrsfi4jwpsma8zknwdmq56hl3nl0z83ch1dh7jh82123hz6mfd8";
      };
      doCheck = false;
    });
in
mkShell {
  buildInputs = [
    dmd
    dub
    lld_13
    openssl.dev
    figlet
  ] ++ lib.optional (! stdenv.isDarwin) [
    (docker.override { buildxSupport = true; })
    docker-compose
    influxdb
    tilt
    kubernetes-helm
    kubernetes
    kind
  ];
  shellHook = ''
    export LD_LIBRARY_PATH=${dmd}/lib:$LD_LIBRARY_PATH
    figlet "Welcome to Jarvis Exchange Price Feed Nix Shell"
  '';
}
